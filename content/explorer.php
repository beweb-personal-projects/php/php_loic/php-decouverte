<?php 
    $current_dir = getCurrentPath();
?>

<div class="content">
    <div class="row justify-content-center d-flex m-sm-3 m-xl-5">
        <?php for($i = 0; $i < count($current_dir); $i++): 
            // Index 0 represents the go back button
            if($i === 0):
                // Inside of the this (if) is verifications and creation of only the go back button.
                if($_GET['page'] === "explorer" && !existsPath()):
                    if($_GET['page'] === "explorer" && existsCurrentDir()):
                        if($i !== 1):
                            getReturnCardView(getPreviousPath(), $current_dir[$i]);
                        endif;
                    endif;
                endif;
            else:
                // Inside of the this (if) is verifications and creation of every directory etc besides "go back".
                if($i > 1):
                    getCardView($current_dir[$i]);
                endif;
            endif;
        endfor;?>
    </div>
</div>