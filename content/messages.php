<?php
    // If the $_SESSION['messages_array'] doesnt exists we will declare and initialize an array inside of the superglobal.
    existsSessionMessages();

    // When we submit the form the data will be added to the $_SESSION['messages_array'] this function will be called.
    addMessageSessionArray();
?>

<div class="container mt-5">
    <table class="table table-dark table-hover">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Username</th>
                <th scope="col">Message</th>
            </tr>
        </thead>
        <tbody>
            <!-- For each message in the Messages_array, we will create a row in the table -->
            <?php 
                foreach($_SESSION['messages_array'] as $i => $message): 
                    getMessageRowView($i, $message['username'], $message['message']);
                endforeach;
            ?>
        </tbody>
    </table>
</div>
