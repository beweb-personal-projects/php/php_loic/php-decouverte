<form class='container' method='POST' action='http://<?=$_SERVER['HTTP_HOST']?>/?page=messages'>
    <div class='card m-5'>
        <div class='card-body'>
            <div class='mb-3'>
                <label for='exampleFormControlInput1' class='form-label'>Email address</label>
                <input type='email' class='form-control' id='exampleFormControlInput1' placeholder='name@example.com' id='email_form' name='email_form'>
            </div>
            <div class='mb-3'>
                <label for='exampleFormControlTextarea1' class='form-label'>Example textarea</label>
                <textarea class='form-control' id='exampleFormControlTextarea1' rows='3' id='text_form' name='text_form'></textarea>
            </div>
            <button type='submit' class='btn btn-primary'>Submit</button>
        </div>
    </div>
</form>