<!-- Load Generic Functions -->
<?php include_once "./functions/generics.php"?>

<?php

    function getReturnCardView($last_dir, $current_dir) {?>
        <div class="card m-2" style="width: 18rem;">
            <img src="https://cdn.arrowpng.com/images/red-go-back-arrow.png" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title text-center">...</h5>
                <div class="row">
                    <?php if(!is_file($current_dir)):?>
                        <a href="http://<?=$_SERVER['HTTP_HOST']?>/?page=explorer&current_dir=<?php 
                            echo ($last_dir !== "") ? $last_dir : ".";
                        ?>" class="btn btn-danger text-center">Go Back</a>
                    <?php endif;?>
                </div>
            </div>
        </div>
    <?php 
    }

    function getCardView($current_dir) {?>
        <div class='card m-2' style='width: 18rem;'>
            <?php if(!strpos($current_dir, '.')):?>
                <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/OneDrive_Folder_Icon.svg/2048px-OneDrive_Folder_Icon.svg.png' class='card-img-top images-explorer' alt='...'>
            <?php else:?>
                <img src='https://i.pinimg.com/originals/7f/d2/e4/7fd2e46b2da9819e667fb75caf475cf7.png' class='card-img-top images-explorer' alt='...'>
            <?php endif;?>

            <div class='card-body'>
                <h5 class='card-title text-center'><?=$current_dir?></h5>
                <div class='row'>
                    <?php if(!strpos($current_dir, '.')):?>
                        <a href='http://<?=$_SERVER['HTTP_HOST']?>/?page=explorer&current_dir=<?php
                            if(isset($_GET['current_dir'])) {
                                echo $_GET['current_dir'] . '/' . $current_dir;
                            } else {
                                echo $current_dir;
                            }
                        ?>' class='btn btn-primary text-center'>Open Folder</a>
                    <?php endif;?>
                </div>
            </div>
        </div>
    <?php
    }

    function getHomeNavbarView($main_page) {?>
        <li class='nav-item mouse-over-navbar'>
            <a class='nav-link' aria-current='page' id='<?=$main_page?>_navbar' href='http://<?=$_SERVER['HTTP_HOST']?>/?page=<?=$main_page?>'><?=ucfirst($main_page)?></a>
        </li>
    <?php
    }

    function getButtonsNavbarView($current_page) {?>
        <li class='nav-item mouse-over-navbar'>
            <a class='nav-link' aria-current='page' id='<?=$current_page?>_navbar' href='http://<?=$_SERVER['HTTP_HOST']?>/?page=<?=$current_page?>'><?=ucfirst($current_page)?></a>
        </li>
    <?php
    }

    function getMessageRowView($id, $username, $message) {?>
        <tr>
            <th scope='row'><?=$id?></th>
            <td><?=$username?></td>
            <td><?=$message?></td>
        </tr>
    <?php
    }

    function getLoginView() {?>
        <form class="d-flex" method='POST'>
            <input class="form-control me-2" type="text" placeholder="Username" name="login_username" aria-label="Search">
            <input class="form-control me-2" type="password" placeholder="Password" name="login_password" aria-label="Search">
            <button class="btn btn-primary me-2" type="submit">Login</button>
        </form>
    <?php
    }

    function getLogoutView() {?>
        <form class="d-flex" method='POST' action='/components/logout.php'>
            <div class="name-session">
                <h5 class="me-2 text-primary"><?=$_COOKIE['session']?></h5>
            </div>
            <button class="btn btn-danger me-2" type="submit">Logout</button>
        </form>
    <?php
    }

?>