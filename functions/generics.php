<?php
    function getCurrentPath() {
        $current_dir = (isset($_GET['current_dir'])) ? $current_dir = scandir('/' . $_GET['current_dir']) : $current_dir = scandir('/');
        return $current_dir;
    }

    function getPreviousPath() {
        $current_dir_array = str_split($_GET['current_dir']);
        $last_dir_index = 0;
        
        for ($j=0; $j < count($current_dir_array); $j++) { 
            if($current_dir_array[$j] === "/") {
                $last_dir_index = $j;
            }
        }

        $last_dir_transform = substr($_GET['current_dir'], $last_dir_index);
        $last_dir = str_replace($last_dir_transform, "", $_GET['current_dir']);

        return $last_dir;
    }

    function existsCurrentDir() {
        // Verification if the current_dir parameter exists
        if(isset($_GET['current_dir'])) return true;
        return false;
    }

    function existsPath() {
        if(existsCurrentDir()) {
            if($_GET['current_dir'] === ".") return true;
            return false;
        }
        return false;
    }

    function removeExtentionName($file) {
        $current_file = substr($file, 0, strrpos($file, "."));
        return $current_file;
    }

    function getPagesPath() {
        $pages_path = scandir("/var/www/html/content");
        return $pages_path;
    }

    function getCurrentPageName() {
        if(isset($_GET['page'])) {
            echo ucfirst($_GET['page']);
        } else {
            echo ucfirst("home");
        }
    }

    function existsSessionMessages() {
        if(!isset($_SESSION['messages_array'])){
            $_SESSION['messages_array'] = [];
        }
    }

    function addMessageSessionArray(){
        if (count($_POST) > 0) {
            array_push($_SESSION['messages_array'], array(
                "username" => $_POST['email_form'],
                "message" => $_POST['text_form'],
            ));
        };
    }

    function addMessageCookieArray(){
        if (count($_COOKIE['MESSAGE_ARRAY']) > 0) {
            $cookie_arr = json_decode($_COOKIE['MESSAGE_ARRAY']);
            var_dump($cookie_arr);

            array_push($cookie_arr, array(
                "username" => $_POST['email_form'],
                "message" => $_POST['text_form'],
            ));

            setcookie("MESSAGE_ARRAY", json_encode($cookie_arr), time() + (86400 * 30), "/");
        };
    }

    function existsSessionToView() {
        if(isset($_COOKIE['session'])) {
            return true;
        }
        return false;
    }
?>