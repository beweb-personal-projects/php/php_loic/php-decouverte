<?php
    $users_array = [
        [
            "username" => "strackz",
            "password" => "aze",
        ],
        [
            "username" => "admin",
            "password" => "qsd",
        ],
    ];


    function verifyUser($users, $username, $password) {
        for ($i=0; $i < count($users); $i++) { 
            if($users[$i]["username"] === $username && $users[$i]["password"] === $password) {
                return true;
            }
        }
        return false;
    }

    function existsSession() {
        if(isset($_COOKIE['session'])) {
            return true;
        }
        return false;
    }

    function existsLoginData() {
        if(isset($_POST['login_username']) && isset($_POST['login_password'])){
            return true;
        }
        return false;
    }

    function notLoggedSafety() {
        if(!isset($_GET['page']) && !existsSession() || $_GET['page'] !== "home" && !existsSession()) {
            header("Location: http://" . $_SERVER['HTTP_HOST'] . "/?page=home");  
        }
    }

    notLoggedSafety();

    if(!existsSession()) {
        if(existsLoginData()){
            if(verifyUser($users_array, $_POST['login_username'], $_POST['login_password'])){
                setcookie("session", $_POST['login_username'], time() + 600);
                header("Location: http://" . $_SERVER['HTTP_HOST'] . "/?page=home");
            }
        }
    }

    session_start(); // Session Start
?>