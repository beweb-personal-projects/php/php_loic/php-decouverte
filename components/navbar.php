<div class="website-navbar">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">PHP - TESTING</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <?php
                    $main_page = "home";
                    $pages = getPagesPath();

                    // The first item will always be the " main " page, in our case we're using home.
                    for ($i=2; $i < count($pages); $i++) { 
                        $current_page = removeExtentionName($pages[$i]);

                        if ($current_page === $main_page) {
                            getHomeNavbarView($main_page); 
                            break;
                        }
                        
                    }

                    // We will add everything different then the main page.
                    for ($i=2; $i < count($pages); $i++) { 
                        if(existsSession()){
                            $current_page = removeExtentionName($pages[$i]);

                            if(strpos($pages[$i], '.')) {
                                if ($current_page !== $main_page) {
                                    getButtonsNavbarView($current_page);
                                }
                            }
                        }
                    }
                ?>
            </ul>
            </div>
        </div>
        <?php
            if(!existsSession()) {
                getLoginView();
            } else {
                getLogoutView();
            }
        ?>
    </nav>
</div>