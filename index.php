<?php 
    // session_set_cookie_params(300); // Session time in seconds, after the seconds are over the session closes itself.
    include "./components/login.php";
?> 
<?php include "./functions/rendering.php"?> <!-- Load Rendering Functions -->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php getCurrentPageName()?></title>
        <?php include("./components/links.php");?>
    </head>
    <body>
        <?php include("./components/navbar.php");?>

        <main class="website-content">
            <?php 
                if(isset($_GET['page'])) {
                    if(is_file("./content/" . strtolower($_GET['page']) . ".php")) {
                        include("./content/" . strtolower($_GET['page']) . ".php");
                    } else {
                        echo "<h2 class='text-center m-5'>ERROR 404 - NOT FOUND</h2>";
                    }
                } else {
                    include("./content/home.php");
                };
            ?>
        </main>

        <?php include("./components/footer.php");?>
    </body>
</html>
